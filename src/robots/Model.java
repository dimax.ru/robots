package robots;

import robots.api.Action;
import robots.api.Brain;
import robots.api.Player;
import robots.api.WorldMap;

import java.util.List;
import java.util.Map;

/**
 * Created by DMX on 16.01.2016.
 */
public class Model {

    private Map<String, Player> players;

    private WorldMap map;

    public void setPlayers(Map<String, Player> players) {
        this.players = players;
    }

    public WorldMap getMap() {
        return map;
    }

    public void setMap(WorldMap map) {
        this.map = map;
    }

    public void applyActions(List<Action> actions) {
        for (Action action : actions) {
            applyAction(action);
        }
    }

    private void applyAction(Action action) {
    }

    public Map<String, Player> getPlayers() {
        return players;
    }
}
