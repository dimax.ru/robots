package robots;

import javax.swing.*;
import java.awt.*;

/**
 * Created by DMX on 16.01.2016.
 */
public class Main {


    public static void main(String[] args) {
        Model model = new Model();
        View view = new View(model);
        Driver driver = new Driver(model, view);

        // place view
        JFrame frame = new JFrame("Robots");
        frame.setContentPane(view);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(500, 400);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }

}
