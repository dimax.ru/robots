package robots.api;

import robots.api.Action;
import robots.api.WorldMap;

import java.util.List;

/**
 * Created by DMX on 16.01.2016.
 */
public interface Brain {

    List<Action> makeMove(WorldMap map);
}
