package robots.api;

/**
 * Created by Yunna on 17.01.2016.
 */
public class Rocket extends MapObject {
    Player player;
    double xPrecise;
    double yPrecise;
    double vx;
    double vy;

    public Rocket(Player player, double x, double y, double vx, double vy) {
        super(player);
        this.player = player;
        this.xPrecise = x;
        this.yPrecise = y;
        this.vx = vx;
        this.vy = vy;
    }

    public Player getPlayer() {
        return player;
    }

    public int getX() {
        return (int) xPrecise;
    }

    public int getY() {
        return (int) yPrecise;
    }

    public double getVx() {
        return vx;
    }

    public double getVy() {
        return vy;
    }

    public double getxPrecise() {
        return xPrecise;
    }

    public double getyPrecise() {
        return yPrecise;
    }
}
