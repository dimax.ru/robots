package robots.api;

import robots.api.MapObject;

/**
 * Created by DMX on 16.01.2016.
 */
public class Robot extends MapObject {

    private int health;

    public Robot(Player player) {
        super(player);
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }
}
