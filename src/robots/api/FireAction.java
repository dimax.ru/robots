package robots.api;

/**
 * Created by DMX on 16.01.2016.
 */
public class FireAction extends Action {

    private Robot robot;

    private int fireX;

    private int fireY;

    public FireAction(Robot robot, int fireX, int fireY) {
        this.robot = robot;
        this.fireX = fireX;
        this.fireY = fireY;
    }

    public Robot getRobot() {
        return robot;
    }

    public int getFireX() {
        return fireX;
    }

    public int getFireY() {
        return fireY;
    }
}
