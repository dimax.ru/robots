package robots.api;

/**
 * Created by DMX on 16.01.2016.
 */
public class BuildAction extends Action {

    private Base base;

    private PlatformType platformType;

    private WeaponType weaponType;

    public BuildAction(Base base, PlatformType platformType, WeaponType weaponType) {
        this.base = base;
        this.platformType = platformType;
        this.weaponType = weaponType;
    }

    public Base getBase() {
        return base;
    }

    public PlatformType getPlatformType() {
        return platformType;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }
}
