package robots.api;

/**
 * Created by DMX on 16.01.2016.
 */
public class MapObject {

    private Player player;

    private int x;

    private int y;

    public MapObject(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
