package robots.api;

import java.util.List;
import java.util.Map;

/**
 * Created by DMX on 16.01.2016.
 */
public class WorldMap {

    private int[][] map;

    private Map<Player, Base> bases;

    private java.util.Map<Player, List<Robot>> robots;

    public int[][] getMap() {
        return map;
    }

    public void setMap(int[][] map) {
        this.map = map;
    }

    public Map<Player, Base> getBases() {
        return bases;
    }

    public void setBases(Map<Player, Base> bases) {
        this.bases = bases;
    }

    public Map<Player, List<Robot>> getRobots() {
        return robots;
    }

    public void setRobots(Map<Player, List<Robot>> robots) {
        this.robots = robots;
    }
}
