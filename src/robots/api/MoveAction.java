package robots.api;

/**
 * Created by DMX on 16.01.2016.
 */
public class MoveAction extends Action {

    private Robot robot;

    private int newX;

    private int newY;

    public MoveAction(Robot robot, int newX, int newY) {
        this.robot = robot;
        this.newX = newX;
        this.newY = newY;
    }

    public Robot getRobot() {
        return robot;
    }

    public int getNewX() {
        return newX;
    }

    public int getNewY() {
        return newY;
    }
}
