package robots.api;

/**
 * Created by DMX on 16.01.2016.
 */
public class Player {

    private Brain brain;

    private int resources;

    public Brain getBrain() {
        return brain;
    }

    public void setBrain(Brain brain) {
        this.brain = brain;
    }

    public int getResources() {
        return resources;
    }

    public void setResources(int resources) {
        this.resources = resources;
    }
}
