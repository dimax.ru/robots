package robots;

import robots.api.*;
import robots.dimka.DimkaBrain;
import robots.kirill.KirillBrain;

import java.util.*;

/**
 * Created by DMX on 16.01.2016.
 */
public class Driver {

    private static final int INIT_RESOURCES = 1000;
    private static final int MAP_WIDTH = 1000;
    private static final int MAP_HEIGHT = 1000;

    public Driver(final Model model, final View view) {
        initModel(model);

        new Thread() {
            public void run() {
                mainLoop(model, view);
            }
        }.start();
    }

    private void initModel(Model model) {
        Player kirill = new Player();
        kirill.setBrain(new DimkaBrain());
        kirill.setResources(INIT_RESOURCES);
        Player dimka = new Player();
        dimka.setBrain(new DimkaBrain());
        dimka.setResources(INIT_RESOURCES);

        Map<String, Player> players = new HashMap<>();
        players.put("Kirill", kirill);
        players.put("Dimka", dimka);
        model.setPlayers(players);

        WorldMap map = new WorldMap();
        map.setMap(new int[MAP_WIDTH][MAP_HEIGHT]);
        map.setBases(null);

        // TODO
        map.setRobots(null);
        model.setMap(map);

    }

    private void mainLoop(Model model, View view) {
        while (true) {
            for (Player player : model.getPlayers().values()) {
                List<Action> actions = player.getBrain().makeMove(model.getMap());
                if (actions != null) {
                    model.applyActions(actions);
                }
            }
            view.repaint();
            try {
                Thread.currentThread().sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
